#include "D3D.h"
#include "SimpleMath.h"
#include "d3dutil.h"
#include "Game.h"
#include "WindowUtils.h"
#include "FX.h"

using namespace DirectX;
using namespace DirectX::SimpleMath;


void Game::BuildQuad()
{
	// Create vertex buffer
	VertexPosNorm vertices[] =
	{	//quad in the XZ plane
		{ Vector3(-1, 0, -1), Vector3(0, 1, 0) },
		{ Vector3(-1, 0, 1), Vector3(0, 1, 0) },
		{ Vector3(1, 0, 1), Vector3(0, 1, 0) },
		{ Vector3(1, 0, -1), Vector3(0, 1, 0) }
	};

	// Create the index buffer
	UINT indices[] = {
		// front face
		0, 1, 2,
		0, 2, 3
	};
	Mesh &mesh = WinUtil::Get().GetD3D().GetMeshMgr().CreateMesh("quad");
	Material mat(Vector4(1, 1, 1, 0), Vector4(0, 1, 0, 0), Vector4(1, 1, 1, 10));
	mesh.CreateFrom(vertices, 4, indices, 6, mat, 0, 6);
	mQuad.Initialise(mesh);
}

void Game::BuildPyramid()
{
	// Create vertex buffer
	VertexPosNorm vertices[] =
	{	//front
		{ Vector3(-1, 0, -1), Vector3(0, 0, 0) },
		{ Vector3(0, 1, 0), Vector3(0, 0, 0) },
		{ Vector3(1, 0, -1), Vector3(0, 0, 0) },
		//left
		{ Vector3(-1, 0, -1), Vector3(0, 0, 0) },
		{ Vector3(0, 0, 1), Vector3(0, 0, 0) },
		{ Vector3(0, 1, 0), Vector3(0, 0, 0) },
		//right
		{ Vector3(1, 0, -1), Vector3(0, 0, 0) },
		{ Vector3(0, 1, 0), Vector3(0, 0, 0) },
		{ Vector3(0, 0, 1), Vector3(0, 0, 0) },
		//bottom
		{ Vector3(-1, 0, -1), Vector3(0, 0, 0) },
		{ Vector3(1, 0, -1), Vector3(0, 0, 0) },
		{ Vector3(0, 0, 1), Vector3(0, 0, 0) }
	};

	// Create the index buffer
	UINT indices[] = {
		// front face
		0, 1, 2,
		3, 4, 5,
		6, 7, 8,
		9, 10, 11
	};

	for (int i = 0; i < 4; ++i)
	{
		int idx = i * 3;
		Vector3 a(vertices[idx].Pos - vertices[idx + 1].Pos), b(vertices[idx + 2].Pos - vertices[idx + 1].Pos);
		a.Normalize();
		b.Normalize();
		vertices[idx].Norm = vertices[idx + 1].Norm = vertices[idx + 2].Norm = b.Cross(a);
	}

	Mesh &mesh = WinUtil::Get().GetD3D().GetMeshMgr().CreateMesh("pyramid");
	Material mat(Vector4(0.1f, 0.1f, 0.1f, 0), Vector4(0.2f, 1, 1, 0), Vector4(0.2f, 0.2f, 0.2f, 1));
	mesh.CreateFrom(vertices, 12, indices, 12, mat, 0, 12);
	mPyramid.Initialise(mesh);
	mPyramid2.Initialise(mesh);
	mPyramid3.Initialise(mesh);
}

void Game::BuildCube()
{
	// Create vertex buffer
	VertexPosNorm vertices[] =
	{	//front
		{ Vector3(-1, -1, -1), Vector3(0, 0, -1) },//0
		{ Vector3(-1, 1, -1), Vector3(0, 0, -1) },//1
		{ Vector3(1, 1, -1), Vector3(0, 0, -1) },//2
		{ Vector3(1, -1, -1), Vector3(0, 0, -1) },//3
		//right
		{ Vector3(1, -1, -1), Vector3(1, 0, 0) },
		{ Vector3(1, 1, -1), Vector3(1, 0, 0) },
		{ Vector3(1, 1, 1), Vector3(1, 0, 0) },
		{ Vector3(1, -1, 1), Vector3(1, 0, 0) },
		//left
		{ Vector3(-1, -1, 1), Vector3(-1, 0, 0) },
		{ Vector3(-1, 1, 1), Vector3(-1, 0, 0) },
		{ Vector3(-1, 1, -1), Vector3(-1, 0, 0) },
		{ Vector3(-1, -1, -1), Vector3(-1, 0, 0) },
		//top
		{ Vector3(-1, 1, -1), Vector3(0, 1, 0) },
		{ Vector3(-1, 1, 1), Vector3(0, 1, 0) },
		{ Vector3(1, 1, 1), Vector3(0, 1, 0) },
		{ Vector3(1, 1, -1), Vector3(0, 1, 0) },
		//bottom
		{ Vector3(1, -1, -1), Vector3(0, -1, 0) },
		{ Vector3(1, -1, 1), Vector3(0, -1, 0) },
		{ Vector3(-1, -1, 1), Vector3(0, -1, 0) },
		{ Vector3(-1, -1, -1), Vector3(0, -1, 0) },
		//back
		{ Vector3(1, -1, 1), Vector3(0, 0, 1) },
		{ Vector3(1, 1, 1), Vector3(0, 0, 1) },
		{ Vector3(-1, 1, 1), Vector3(0, 0, 1) },
		{ Vector3(-1, -1, 1), Vector3(0, 0, 1) }
	};

	// Create the index buffer
	UINT indices[] = {
		// front face
		0, 1, 2,
		0, 2, 3,
		//right
		4, 5, 6,
		4, 6, 7,
		//left
		8, 9, 10,
		8, 10, 11,
		//top
		12, 13, 14,
		12, 14, 15,
		//bottom
		16, 17, 18,
		16, 18, 19,
		//back
		20, 21, 22,
		20, 22, 23
	};

	Mesh &mesh = WinUtil::Get().GetD3D().GetMeshMgr().CreateMesh("box");
	Material mat(Vector4(1, 1, 1, 0), Vector4(1, 0, 0, 0), Vector4(1, 1, 1, 10));
	mesh.CreateFrom(vertices, 24, indices, 36, mat, 0, 36);

	//Ufo Code

}

Mesh& Game::BuildSphere()
{
	int LatLines = 17, LongLines = 17;
	int numSphereVert = ((LatLines - 2) * LongLines) + 2;
	int numSphereFace = ((LatLines - 3)*(LongLines) * 2) + (LongLines * 2);

	float sphereYaw = 0.0f;
	float spherePitch = 0.0f;

	std::vector<VertexPosNorm> vertices(numSphereVert);
	Vector3 currentVertPos(0.0f, 0.0f, 1.0f);
	vertices[0].Pos = Vector3(0, 0, 1);

	for (int i = 0; i < LatLines - 2; i++)
	{
		spherePitch = (i + 1) * (PI / (LatLines - 1));
		Matrix rotationx = Matrix::CreateRotationX(spherePitch);
		
		for (int j = 0; j < LongLines; j++)
		{
			sphereYaw = j * ((2 * PI) / (LongLines));
			Matrix rotationy = Matrix::CreateRotationZ(sphereYaw);
			currentVertPos = Vector3::TransformNormal(Vector3(0, 0, 1), rotationx * rotationy);
			currentVertPos.Normalize();
			int idx = i * LongLines + j + 1;
			vertices[idx].Pos = currentVertPos;
			vertices[idx].Norm = currentVertPos;
			vertices[idx].Norm.Normalize();
		}
	}

	vertices[numSphereVert - 1].Pos = Vector3(0, 0, -1);
	std::vector<unsigned int> indices(numSphereFace * 3);

	int k = 0;
	for (int l = 0; l < LongLines - 1; l++)
	{
		indices[k] = 0;
		indices[k + 2] = l + 1;
		indices[k + 1] = l + 2;
		k += 3;
	}

	indices[k] = 0;
	indices[k + 2] = LongLines;
	indices[k + 1] = 1;
	k += 3;

	for (int i = 0; i < LatLines - 3; i++)
	{
		for (int j = 0; j < LongLines - 1; j++)
		{
			indices[k] = i * LongLines + j + 1;
			indices[k + 1] = i * LongLines + j + 2;
			indices[k + 2] = (i + 1)*LongLines + j + 1;

			indices[k + 3] = (i + 1)*LongLines + j + 1;
			indices[k + 4] = i * LongLines + j + 2;
			indices[k + 5] = (i + 1)*LongLines + j + 2;

			k += 6;
		}

		indices[k] = (i*LongLines) + LongLines;
		indices[k + 1] = (i*LongLines) + 1;
		indices[k + 2] = ((i + 1)*LongLines) + LongLines;

		indices[k + 3] = ((i + 1)*LongLines) + LongLines;
		indices[k + 4] = (i*LongLines) + 1;
		indices[k + 5] = ((i + 1)*LongLines) + 1;

		k += 6;
	}

	for (int l = 0; l < LongLines - 1; ++l)
	{
		indices[k] = numSphereVert - 1;
		indices[k + 2] = (numSphereVert - 1) - (l + 1);
		indices[k + 1] = (numSphereVert - 1) - (l + 2);
		k += 3;
	}

	indices[k] = numSphereVert - 1;
	indices[k + 2] = (numSphereVert - 1) - LongLines;
	indices[k + 1] = numSphereVert - 2;

	Mesh &mesh = WinUtil::Get().GetD3D().GetMeshMgr().CreateMesh("sphere");
	Material mat(Vector4(1, 1, 1, 0), Vector4(1, 0, 0, 0), Vector4(1, 1, 1, 10));
	mesh.CreateFrom(&vertices[0], numSphereVert, &indices[0], numSphereFace * 3, mat, 0, numSphereFace * 3);

	mUfo.Initialise(mesh);
	mLight = mUfo;
	return mesh;
}

void Game::Initialise()
{
	BuildQuad();
	BuildCube();
	BuildPyramid();
	BuildSphere();

	mLight.GetScale() = Vector3(0.025f);

	mQuad.GetScale() = Vector3(3, 1, 3);
	mQuad.GetPosition() = Vector3(0, -1, 0);


	//Pyramid 1 Code
	mPyramid.GetScale() = Vector3(1.f, 1.f, 1.f);
	mPyramid.GetPosition() = Vector3(1.2f, -0.1f, 0.55f);

	//Pyramid 2 code
	mPyramid2.GetPosition() = Vector3(-2.4f, -0.4f, -1.55f);
	mPyramid2.GetRotation().y = -72.f;

	//Pyramid 3 code
	mPyramid3.GetPosition() = Vector3(2.2f, -1.f, -1.f);
	mPyramid3.GetRotation().y = 20.f;

	//Ufo code
	mUfo.GetScale() = Vector3(0.25f);

	FX::SetupDirectionalLight(0, true, Vector3(0.7f, -0.7f, 0.7f), Vector3(0.8f, 0.4f, 0.4f), Vector3(1, 1, 1), Vector3(0.2f, 0.1f, 0.05f));
}

void Game::Release()
{
}

void Game::Update(float dTime)
{
	gAngle += dTime * 0.5f;
}

void Game::Render(float dTime)
{
	MyD3D& d3d = WinUtil::Get().GetD3D();
	d3d.BeginRender(Colours::Black);

	float alpha = 0.5f + sinf(gAngle * 2)*0.5f;

	//UFO
	mUfo.GetPosition() = Vector3(-4 + alpha * 4, 0.5, 1);
	Vector3 ufoLight = Vector3(alpha, 0.9f, 0.8f);
	FX::SetupPointLight(3, true, mUfo.GetPosition(), ufoLight, Vector3(0.1f, 0.2f, 0.2f), Vector3(0, 0, 0), 2.5, 4.f);
	mUfo.GetMesh().GetSubMesh(0).material.Specular = Vec3To4(ufoLight, 0);
	d3d.GetFX().Render(mUfo, FX::FxFlags::NONE);

	FX::SetPerFrameConsts(d3d.GetDeviceCtx(), mCamPos);

	CreateViewMatrix(FX::GetViewMatrix(), mCamPos, Vector3(0, 0, 0), Vector3(0, 1, 0));
	CreateProjectionMatrix(FX::GetProjectionMatrix(), 0.25f*PI, WinUtil::Get().GetAspectRatio(), 1, 1000.f);
	Matrix w = Matrix::CreateRotationY(sinf(gAngle));
	FX::SetPerObjConsts(d3d.GetDeviceCtx(), w);


	Material mat = { { 1, 1, 1, 0 }, { 1, 1, 1, 0 }, { 0, 0, 0, 1 } };

	//Pyramids
	d3d.GetFX().Render(mPyramid, FX::FxFlags::NONE, &mat);
	d3d.GetFX().Render(mPyramid2, FX::FxFlags::NONE, &mat);
	d3d.GetFX().Render(mPyramid3, FX::FxFlags::NONE, &mat);

	//floor
	mat.Diffuse = Vector4(0.4f, 1, 0.4f, 0);
	d3d.GetFX().Render(mQuad, FX::FxFlags::LIT, &mat);

	d3d.EndRender();

}

LRESULT Game::WindowsMssgHandler(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	//do something game specific here
	switch (msg)
	{
		// Respond to a keyboard event.
	case WM_CHAR:
		switch (wParam)
		{
		case 27:
		case 'q':
		case 'Q':
			PostQuitMessage(0);
			return 0;
		}
	}
	//default message handling (resize window, full screen, etc)
	return WinUtil::Get().DefaultMssgHandler(hwnd, msg, wParam, lParam);
}

